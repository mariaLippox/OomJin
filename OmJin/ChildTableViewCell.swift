//
//  ChildTableViewCell.swift
//  OmJin
//
//  Created by Maria Hientono on 3/30/16.
//  Copyright © 2016 Lalulalilo. All rights reserved.
//

import UIKit

class ChildTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  FirstViewController.swift
//  OmJin
//
//  Created by Maria Hientono on 3/28/16.
//  Copyright © 2016 Lalulalilo. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Actions
    @IBAction func testButtonPressed() {
        print("testButtonPressed")
        print("ArrayOfNames: \(NSUserDefaults.standardUserDefaults().objectForKey("ArrayOfNames")!) ")
    }
    
    @IBAction func saveButtonPressed() {
        
        let userDefault = NSUserDefaults.standardUserDefaults()
        var namesArray : [Dictionary<String, String>] = []
        
        if(userDefault.objectForKey("ArrayOfNames") == nil) {
            userDefault.setObject(namesArray, forKey: "ArrayOfNames")
        }
        
        else {
            namesArray = userDefault.objectForKey("ArrayOfNames") as! [Dictionary<String, String>]
        }
        
        var namesDictionary : Dictionary <String, String> = [:]
        
        namesDictionary["firstName"] = self.firstNameTextField.text!
        namesDictionary["lastName"] = self.lastNameTextField.text!
        
        namesArray.append(namesDictionary)
        
        userDefault.setObject(namesArray, forKey: "ArrayOfNames")
        
        self.firstNameTextField.text = ""
        self.lastNameTextField.text = ""
    }
    
    //MARK: - Properties and Outlets
    @IBOutlet weak var testButton: UIButton!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!

}


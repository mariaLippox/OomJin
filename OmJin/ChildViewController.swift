//
//  ChildViewController.swift
//  OmJin
//
//  Created by Maria Hientono on 3/30/16.
//  Copyright © 2016 Lalulalilo. All rights reserved.
//

import UIKit

class ChildViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Action Methods
    @IBAction func backButtonPressed() {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    
    //MARK: - TableViewDelegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (NSUserDefaults.standardUserDefaults().objectForKey("ArrayOfNames") as! [Dictionary<String, String>]).count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("childCell")! as! ChildTableViewCell
        
        let nameArray = NSUserDefaults.standardUserDefaults().objectForKey("ArrayOfNames") as! [Dictionary<String, String>]
        
        cell.name.text = nameArray[indexPath.row]["firstName"]! + " " + nameArray[indexPath.row]["lastName"]!
        
        return cell
    }


    //MARK: Properties and Outlets
    @IBOutlet weak var tableView: UITableView!
    
    
    

}
